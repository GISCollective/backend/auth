module ogm.serverauthentication.auth;

import ogm.auth;

import std.stdio;
import std.path;
import std.file;

import crate.auth.usercollection;
import crate.base;
import crate.http.router;
import crate.auth.middleware;

import vibeauth.challenges.recaptcha;
import vibeauth.mail.vibe;
import vibeauth.authenticators.OAuth2;

import vibe.http.router;

import ogm.crates.all;
import ogm.authservice.configuration;
import ogm.authservice.preferences;
import ogm.authservice.ajax;
import ogm.middleware.authsession;

///
class ServerAuthentication : Authentication {

  private {
    AuthConfiguration configuration;
    OgmCrates crates;
  }

  ///
  this(OgmCrates crates, AuthConfiguration configuration) {
    super(crates.user, []);
    this.configuration = configuration;
    this.crates = crates;
  }

  ///
  override OAuth2 oAuth2() {
    return super.oAuth2;
  }

  ///
  void bindRoutes(URLRouter router) {
    auto emailConfig = configuration.getEmailConfiguration;
    emailConfig.smtp = new SMTPConfig(crates);

    auto mailQueue = new VibeMailQueue(emailConfig);
    auto service = configuration.parseOauthService;

    auto ajaxAuth = new AjaxAuth(crates, getCollection, configuration);
    auto authSession = new AuthSession(getCollection);

    router.match(HTTPMethod.OPTIONS, "/forgotpassword", &ajaxAuth.options);
    router.post("/forgotpassword", &ajaxAuth.cors);
    router.post("/forgotpassword", &ajaxAuth.createResetToken);

    router.match(HTTPMethod.OPTIONS, "/resetpassword", &ajaxAuth.options);
    router.post("/resetpassword", &ajaxAuth.cors);
    router.post("/resetpassword", &ajaxAuth.resetPassword);

    router.match(HTTPMethod.OPTIONS, "/registerchallenge", &ajaxAuth.options);
    router.post("/registerchallenge", &ajaxAuth.cors);
    router.post("/registerchallenge", &ajaxAuth.registerChallenge);

    router.match(HTTPMethod.OPTIONS, "/register", &ajaxAuth.options);
    router.post("/register", &ajaxAuth.cors);
    router.post("/register", &ajaxAuth.register);

    router.match(HTTPMethod.OPTIONS, "/activate", &ajaxAuth.options);
    router.post("/activate", &ajaxAuth.cors);
    router.post("/activate", &ajaxAuth.activate);
  }
}
