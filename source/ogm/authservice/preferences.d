module ogm.authservice.preferences;

import ogm.crates.all;
import vibeauth.challenges.recaptcha;
import vibeauth.challenges.mtcaptcha;
import vibeauth.mail.base;
import vibe.core.log;
import std.conv;

class ConfigPreferences {

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  string get(string name) {
    return this.crates.preference.get.where("name").equal(name).and.exec.front["value"].to!string;
  }
}

class ReCaptchaConfig : IReCaptchaConfig {

  private {
    ConfigPreferences preferences;
  }

  this(OgmCrates crates) {
    this.preferences = new ConfigPreferences(crates);
  }

  string siteKey() {
    return this.preferences.get("secret.recaptcha.siteKey");
  }

  string secretKey() {
    return this.preferences.get("secret.recaptcha.secretKey");
  }
}

class MtCaptchaConfig : IMtCaptchaConfig {

  private {
    ConfigPreferences preferences;
  }

  this(OgmCrates crates) {
    this.preferences = new ConfigPreferences(crates);
  }

  string siteKey() {
    return this.preferences.get("secret.mtcaptcha.siteKey");
  }

  string privateKey() {
    return this.preferences.get("secret.mtcaptcha.privateKey");
  }
}

class SMTPConfig : ISMTPConfig {

  private {
    OgmCrates crates;
  }

  this(OgmCrates crates) {
    this.crates = crates;
  }

  private string get(string key)() {
    return this.crates.preference.get.where("name").equal("secret.smtp." ~ key).and.exec.front["value"].to!string;
  }

  string authType() {
    return this.get!"authType";
  }

  string connectionType() {
    return this.get!"connectionType";
  }

  string tlsValidationMode() {
    return this.get!"tlsValidationMode";
  }

  string tlsVersion() {
    return this.get!"tlsVersion";
  }

  string host() {
    return this.get!"host";
  }

  ushort port() {
    string value;

    try {
      value = this.get!"port".to!string;

      return value.to!ushort;
    } catch(Exception e) {
      logError("Can't get the smpt port value '%s': %s", value, e.toString);
    }

    return 465;
  }

  string localname() {
    return this.get!"localname";
  }

  string password() {
    return this.get!"password";
  }

  string username() {
    return this.get!"username";
  }

  string from() {
    return this.get!"from";
  }
}