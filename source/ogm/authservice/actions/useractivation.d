module ogm.authservice.actions.useractivation;

import vibe.data.json;

import std.datetime;
import std.exception;
import std.algorithm;

import vibeauth.collections.usermemory;
import vibeauth.collections.usercollection;
import vibeauth.data.usermodel;
import vibeauth.data.token;
import vibeauth.data.user;

version(unittest) {
  import fluent.asserts;
}

/// Activate an user account
struct UserActivation {
  /// The users email that will be activated
  string email;

  /// The activation token
  string token;

  /// Collection with the service users
  UserCollection userCollection;

  /// validation function
  void delegate(const UserModel) validation;

  ///
  void activate() {
    enforce(email != "", "The email was not set.");
    enforce(userCollection.contains(email), "The email `" ~ email ~ "` was not found.");
    enforce(!userCollection[email].isActive, "The account associated to `" ~ email ~ "` is active.");

    auto user = userCollection[email];

    enforce(user.getTokensByType("activation").filter!(a => a.expire > Clock.currTime).map!(a => a.name).canFind(token), "Invalid activation token.");

    if(validation !is null) {
      validation(user.toJson.deserializeJson!UserModel);
    }

    user.isActive = true;

    user
      .getTokensByType("activation")
      .map!(a => a.name)
      .each!(a => user.revoke(a));
  }
}

/// Throw an exception when the email is not set
unittest {
  auto userActivation = UserActivation("");
  userActivation.userCollection = new UserMemoryCollection([]);

  ({
    userActivation.activate();
  }).should.throwAnyException.withMessage("The email was not set.");
}

/// Throw an exception when the email does not exist
unittest {
  auto userActivation = UserActivation("test@test.com");
  userActivation.userCollection = new UserMemoryCollection([]);

  ({
    userActivation.activate();
  }).should.throwAnyException.withMessage("The email `test@test.com` was not found.");
}

/// Throw an exception when the user is active
unittest {
  auto userActivation = UserActivation("test@test.com");
  userActivation.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  user.isActive = true;
  userActivation.userCollection.add(user);

  ({
    userActivation.activate();
  }).should.throwAnyException.withMessage("The account associated to `test@test.com` is active.");
}

/// Activate the user
unittest {
  auto userActivation = UserActivation("test@test.com");
  userActivation.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  user.isActive = false;
  userActivation.userCollection.add(user);

  auto token = userActivation.userCollection.createToken("test@test.com", Clock.currTime + 3600.seconds, [], "activation");
  userActivation.token = token.name;

  userActivation.activate();

  userActivation.userCollection["test@test.com"].isActive.should.equal(true);
  userActivation.userCollection["test@test.com"].isValidToken(token.name).should.equal(false);
}

/// Send the user data to a validation function if it is set
unittest {
  void mockValidation(const UserModel) {
    throw new Exception("Validation failed.");
  }

  auto userActivation = UserActivation("test@test.com");
  userActivation.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  user.isActive = false;
  userActivation.userCollection.add(user);

  auto token = userActivation.userCollection.createToken("test@test.com", Clock.currTime + 3600.seconds, [], "activation");
  userActivation.token = token.name;

  userActivation.validation = &mockValidation;

  ({
    userActivation.activate();
  }).should.throwAnyException.withMessage("Validation failed.");
}

/// Throw exception when the token is invalid
unittest {
  auto userActivation = UserActivation("test@test.com");
  userActivation.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  user.isActive = false;
  userActivation.userCollection.add(user);

  auto token = userActivation.userCollection.createToken("test@test.com", Clock.currTime + 3600.seconds, [], "other");
  userActivation.token = token.name;

  ({
    userActivation.activate();
  }).should.throwAnyException.withMessage("Invalid activation token.");
}

/// Throw exception when the token is expired
unittest {
  auto userActivation = UserActivation("test@test.com");
  userActivation.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  user.isActive = false;
  userActivation.userCollection.add(user);

  auto token = userActivation.userCollection.createToken("test@test.com", Clock.currTime - 1.seconds, [], "activation");
  userActivation.token = token.name;

  ({
    userActivation.activate();
  }).should.throwAnyException.withMessage("Invalid activation token.");
}
