module ogm.authservice.actions.activatetoken;

import vibe.data.json;

import std.datetime;
import std.exception;
import std.algorithm;

import vibeauth.collections.usercollection;
import vibeauth.data.usermodel;
import vibeauth.data.token;
import vibeauth.collections.usermemory;
import vibeauth.data.user;

version(unittest) {
  import fluent.asserts;
}

/// Create a token that allows an user to activate the account
struct ActivateToken {
  /// The users email that the token will be created
  string email;

  /// Collection with the service users
  UserCollection userCollection;

  /// validation function
  void delegate(const UserModel) validation;

  ///
  Token create() {
    enforce(email != "", "The email was not set.");
    enforce(userCollection.contains(email), "The email `" ~ email ~ "` was not found.");
    enforce(!userCollection[email].isActive, "The account associated to `" ~ email ~ "` is active.");

    auto user = userCollection[email];

    if(validation !is null) {
      validation(user.toJson.deserializeJson!UserModel);
    }

    user
      .getTokensByType("activation")
      .map!(a => a.name)
      .each!(a => user.revoke(a));

    return userCollection.createToken(email, Clock.currTime.toUTC + 2.days, [], "activation");
  }
}

/// Throw an exception when the email is not set
unittest {
  auto activateToken = ActivateToken("");
  activateToken.userCollection = new UserMemoryCollection([]);

  ({
    activateToken.create();
  }).should.throwAnyException.withMessage("The email was not set.");
}

/// Throw an exception when the email does not exist
unittest {
  auto activateToken = ActivateToken("test@test.com");
  activateToken.userCollection = new UserMemoryCollection([]);

  ({
    activateToken.create();
  }).should.throwAnyException.withMessage("The email `test@test.com` was not found.");
}

/// Throw an exception when the user is active
unittest {
  auto activateToken = ActivateToken("test@test.com");
  activateToken.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  user.isActive = true;
  activateToken.userCollection.add(user);

  ({
    activateToken.create();
  }).should.throwAnyException.withMessage("The account associated to `test@test.com` is active.");
}

/// Generate a new token
unittest {
  auto activateToken = ActivateToken("test@test.com");
  activateToken.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  activateToken.userCollection.add(user);

  auto token = activateToken.create();
  token.name.should.not.equal("");
  token.type.should.equal("activation");
  token.expire.should.be.greaterThan(Clock.currTime + 1.hours - 1.seconds);
}

/// Send the user data to a validation function if it is set
unittest {
  void mockValidation(const UserModel) {
    throw new Exception("Validation failed.");
  }

  auto activateToken = ActivateToken("test@test.com");
  activateToken.userCollection = new UserMemoryCollection([]);
  activateToken.validation = &mockValidation;

  auto user = new User("test@test.com", "password");
  activateToken.userCollection.add(user);

  ({
    activateToken.create();
  }).should.throwAnyException.withMessage("Validation failed.");
}

/// Remove existing activation tokens
unittest {
  auto activateToken = ActivateToken("test@test.com");
  activateToken.userCollection = new UserMemoryCollection([]);

  auto user = new User("test@test.com", "password");
  activateToken.userCollection.add(user);

  auto existingToken = activateToken.userCollection.createToken("test@test.com", Clock.currTime + 3600.seconds, [], "activation");

  auto token = activateToken.create();

  activateToken.userCollection["test@test.com"].isValidToken(existingToken.name).should.equal(false);
}
