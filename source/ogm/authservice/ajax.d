module ogm.authservice.ajax;

import ogm.crates.all;
import crate.error;

import vibe.http.router;
import vibe.core.core;
import vibe.data.json;
import vibe.core.log;

import vibeauth.mail.base;
import vibeauth.mail.vibe;
import vibeauth.challenges.base;
import vibeauth.challenges.recaptcha;
import vibeauth.challenges.mtcaptcha;

import crate.auth.usercollection;
import crate.http.cors;

import std.datetime;
import std.exception;
import std.algorithm;
import std.random;
import std.string;

import ogm.authservice.configuration;
import ogm.authservice.validation;

import ogm.authservice.actions.createresettoken;
import ogm.authservice.actions.resetpassword;
import ogm.authservice.actions.register;
import ogm.authservice.actions.activatetoken;
import ogm.authservice.actions.useractivation;
import ogm.authservice.preferences;
import vibe.service.stats;

class AjaxAuth {

  private {
    UserCrateCollection userCollection;
    VibeMailQueue mailQueue;
    OgmCrates crates;
    UserDataValidation validator;

    string serviceUrl;
    string serverUrl;
  }

  ///
  this(OgmCrates crates, UserCrateCollection userCollection, AuthConfiguration configuration) {
    auto emailConfig = configuration.getEmailConfiguration;
    emailConfig.smtp = new SMTPConfig(crates);

    this.userCollection = userCollection;
    this.mailQueue = new VibeMailQueue(emailConfig);
    this.crates = crates;
    this.serviceUrl = configuration.general.serviceUrl;
    this.serverUrl = configuration.general.serviceUrl;
    this.validator = new UserDataValidation(crates.preference);
  }

  IChallenge challenge() {
    if(this.captchaType == "reCAPTCHA") {
      return new ReCaptcha(new ReCaptchaConfig(crates));
    }

    if(this.captchaType == "mtCAPTCHA") {
      return new MtCaptcha(new MtCaptchaConfig(crates));
    }

    return null;
  }

  string captchaType() {
    auto preference = crates.preference.get
      .where("name").equal("captcha.enabled")
      .and.exec.front;

    return preference["value"].to!string;
  }

  bool hasCaptcha() {
    return this.captchaType == "mtCAPTCHA" || this.captchaType == "reCAPTCHA";
  }

  void cors(HTTPServerRequest req, HTTPServerResponse response) {
    response.addHeaderValue("Access-Control-Allow-Origin", ["*"]);
    response.addHeaderValue("Access-Control-Allow-Methods", ["POST"]);
    response.addHeaderValue("Access-Control-Allow-Headers", ["Content-Type", "Authorization"]);
  }

  void options(HTTPServerRequest req, HTTPServerResponse response) {
    cors(req, response);

    response.writeBody("", 204);
  }

  void createResetToken(HTTPServerRequest req, HTTPServerResponse res) {
    Stats.instance.inc("auth_create_reset_token", ["result": "start"]);

    try {
      string email = req.json["email"].to!string.strip;

      auto createResetToken = CreateResetToken(email);
      createResetToken.userCollection = userCollection;
      createResetToken.validation = &validator.validate;

      auto token = createResetToken.create();

      auto variables = serviceVariables(email);
      mailQueue.addResetPasswordMessage(variables["user.email"], token, variables);

      res.writeBody("", 204);
      Stats.instance.inc("auth_create_reset_token", ["result": "success"]);
    } catch(Exception e) {
      Stats.instance.inc("auth_create_reset_token", ["result": "failure"]);
      sleep(uniform(0, 1000).msecs);
      logError(e.message);
    }
  }

  void resetPassword(HTTPServerRequest req, HTTPServerResponse res) {
      Stats.instance.inc("auth_reset_password", ["result": "start"]);

    try {
      string email = req.json["email"].to!string.strip;
      string token = req.json["token"].to!string.strip;
      string password = req.json["password"].to!string.strip;

      auto resetPassword = ResetPassword(email, password, token);
      resetPassword.userCollection = userCollection;
      resetPassword.validation = &validator.validate;

      resetPassword.changePassword();

      auto variables = serviceVariables(email);
      mailQueue.addResetPasswordConfirmationMessage(variables["user.email"], variables);

      res.writeBody("", 204);
      Stats.instance.inc("auth_reset_password", ["result": "success"]);
    } catch(Exception e) {
      Stats.instance.inc("auth_reset_password", ["result": "failure"]);
      sleep(uniform(0, 1000).msecs);
      logError(e.message);

      string msg = `{"errors":[{"description":"` ~ e.message.idup ~ `","status":400,"title":"Validation error"}]}`;
      res.writeBody(msg, 400, "text/json");
    }
  }

  void register(HTTPServerRequest req, HTTPServerResponse res) {
    Stats.instance.inc("auth_register", ["result": "start"]);

    try {
      Register register;
      register.salutation = req.json["salutation"].to!string.strip;
      register.title = req.json["title"].to!string.strip;
      register.firstName = req.json["firstName"].to!string.strip;
      register.lastName = req.json["lastName"].to!string.strip;
      register.username = req.json["username"].to!string.strip;
      register.email = req.json["email"].to!string.strip;
      register.password = req.json["password"].to!string.strip;
      register.challengeResponse = req.json["challenge"].to!string.strip;

      register.userCollection = userCollection;
      register.validation = &validator.validateRegistration;
      register.challenge = challenge;

      auto token = register.createUser();

      auto variables = serviceVariables(register.email);
      mailQueue.addActivationMessage(variables["user.email"], token, variables);

      res.writeBody("", 204);
      Stats.instance.inc("auth_register", ["result": "success"]);
    } catch(Exception e) {
      Stats.instance.inc("auth_register", ["result": "failure"]);
      sleep(uniform(0, 1000).msecs);
      logError(e.message);

      string msg = `{"errors":[{"description":"` ~ e.message.idup ~ `","status":400,"title":"Validation error"}]}`;
      res.writeBody(msg, 400, "text/json");
    }
  }

  void registerChallenge(HTTPServerRequest req, HTTPServerResponse res) {
    Stats.instance.inc("auth_register_challenge");

    Json data;

    if(hasCaptcha) {
      data = this.challenge.getConfig();
    } else {
      data = Json.emptyObject;
    }

    res.writeBody(data.toPrettyString, 200);
  }

  void activate(HTTPServerRequest req, HTTPServerResponse res) {
    Stats.instance.inc("auth_activate", ["result": "start"]);

    try {
      if("token" !in req.json) {
        req.json["token"] = "";
      }

      string email = req.json["email"].to!string.strip;
      string token = req.json["token"].to!string.strip;

      if(token == "") {
        auto activateToken = ActivateToken(email);
        activateToken.userCollection = userCollection;
        activateToken.validation = &validator.validateRegistration;

        auto newToken = activateToken.create();
        auto variables = serviceVariables(email);

        enforce("user.email" in variables, "The user email is not set.");

        mailQueue.addActivationMessage(variables["user.email"], newToken, variables);
      }

      if(token != "") {
        auto userActivation = UserActivation(email, token);
        userActivation.userCollection = userCollection;
        userActivation.validation = &validator.validateRegistration;
        userActivation.activate();
      }

      Stats.instance.inc("auth_activate", ["result": "success"]);
    } catch(Exception e) {
      Stats.instance.inc("auth_activate", ["result": "failure"]);

      sleep(uniform(0, 1000).msecs);
      logError(e.message);

      string msg = `{"errors":[{"description":"Invalid request.","status":400,"title":"Validation error"}]}`;
      res.writeBody(msg, 400, "text/json");
    }


    res.writeBody("", 204);
  }

  string[string] serviceVariables(string email) {
    string[string] variables;

    variables["serviceUrl"] = serviceUrl;
    variables["serverUrl"] = serverUrl;

    variables["serviceName"] = crates.preference.get.where("name")
      .equal("appearance.name")
      .and.exec.front["value"]
        .to!string;

    scope user = userCollection[email];
    variables["user.title"] = user.title;
    variables["user.salutation"] = user.salutation;
    variables["user.firstName"] = user.firstName;
    variables["user.lastName"] = user.lastName;
    variables["user.name"] = user.firstName ~ " " ~ user.lastName;
    variables["user.email"] = user.email;

    if(user.firstName.strip == "") {
      variables["user.firstName"] = user.username;
      variables["user.name"] = user.username;
    }

    return variables;
  }
}
